ocaml-getopt (0.0.20040811-10) unstable; urgency=low

  [ Stéphane Glondu ]
  * Switch packaging to git

  [ Mehdi Dogguy ]
  * Refresh packaging:
    - Use debhelper (>= 7.0.50~)
    - Updated standards version to 3.8.3
    - Use new features of dh-ocaml (>= 0.9~)
  * Install debian/libgetopt-ocaml-dev.doc-base by hand instead of
    using ocamldoc-api-ref-config (Closes: #549796)

 -- Mehdi Dogguy <mehdi@debian.org>  Thu, 08 Oct 2009 18:33:02 +0200

ocaml-getopt (0.0.20040811-9) unstable; urgency=low

  [ Stefano Zacchiroli ]
  * fix vcs-svn field to point just above the debian/ dir

  [ Mehdi Dogguy ]
  * Add dh-ocaml as a build-dependency
  * Use new Ocaml's standard library location
  * Updated standards version to 3.8.2
  * Move the package to section ocaml
  * Move maintainer to uploaders and mark d-o-m as maintainer
  * Add myself to uploaders
  * Build-depend on ocaml 3.11.1
  * Add a watch file
  * Add a homepage field
  * Add a copyright notice
  * Register documentation with docbase

 -- Mehdi Dogguy <dogguy@pps.jussieu.fr>  Wed, 01 Jul 2009 11:44:35 +0200

ocaml-getopt (0.0.20040811-8) unstable; urgency=low

  * rebuild with 3.10.0

 -- Mike Furr <mfurr@debian.org>  Fri, 07 Sep 2007 17:14:07 -0400

ocaml-getopt (0.0.20040811-7) unstable; urgency=low

  * Install html documentation, thanks to Georg Neis.

 -- Mike Furr <mfurr@debian.org>  Thu,  1 Feb 2007 16:28:06 -0500

ocaml-getopt (0.0.20040811-6) unstable; urgency=low

  * Install the getopt.mli file (Closes: #387131)
  * Removed control.in per new policy
  * Updated standards version to 3.7.2 (no changes)
  * changed to svn-buildpackage compatible layout in svn

 -- Mike Furr <mfurr@debian.org>  Thu, 14 Sep 2006 10:45:34 -0400

ocaml-getopt (0.0.20040811-5) unstable; urgency=low

  * Oops, missed one hard-coded path which caused everything 
    to be installed into 3.09.0 instead of .1

 -- Mike Furr <mfurr@debian.org>  Tue, 17 Jan 2006 20:21:48 -0500

ocaml-getopt (0.0.20040811-4) unstable; urgency=low

  * Remove hard-coded abi strings
  * Rebuild with 3.09.1

 -- Mike Furr <mfurr@debian.org>  Sun,  8 Jan 2006 13:30:50 -0500

ocaml-getopt (0.0.20040811-3) unstable; urgency=low

  * Rebuild with 3.09.0

 -- Mike Furr <mfurr@debian.org>  Fri, 25 Nov 2005 13:38:49 -0500

ocaml-getopt (0.0.20040811-2) unstable; urgency=low

  * Added depends on ocaml-nox-3.08.3 to ensure compatibility. 
    (Closes: 306074)

 -- Mike Furr <mfurr@debian.org>  Sun, 24 Apr 2005 13:04:57 -0400

ocaml-getopt (0.0.20040811-1) unstable; urgency=low

  * Initial Release (Closes: 303971).

 -- Mike Furr <mfurr@debian.org>  Sat,  9 Apr 2005 18:00:56 -0400
